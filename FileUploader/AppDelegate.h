//
//  AppDelegate.h
//  FileUploader
//
//  Created by Chang Liu on 6/22/18.
//  Copyright © 2018 Next Entertainment. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

