//
//  ViewController.m
//  FileUploader
//
//  Created by Chang Liu on 6/22/18.
//  Copyright © 2018 Next Entertainment. All rights reserved.
//

#import "ViewController.h"
#import "NEFileUploaderManager.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [[NEFileUploaderManager shareManager] setCustomCachePath:@"文件目录"];
    NSLog(@"path = %@",[NEFileUploaderManager fileCachePath]);
    
    UIView *testProgressView = [[UIView alloc] initWithFrame:CGRectMake(0, 100, 0, 20)];
    [testProgressView setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:testProgressView];
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DSC07510" ofType:@"ARW"];
    NSString *filePath = @"/Users/Alienchang/Downloads/DSC07495.ARW";
    NEFileUploadMission *mission = [NEFileUploadMission missionWithTargetUrl:@"www.baidu.com" filePath:filePath uploadProgress:^(float speed, float progress) {
        if (progress >= 1) {
            
        }
        [UIView animateWithDuration:0.5 animations:^{
            [testProgressView setFrame:CGRectMake(0, 100, 200 * progress, 20)];
        }];
    }];
    
    [NEFileUploaderManager testCopy:mission];
//    [NEFileUploaderManager uploadWithMission:mission];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
