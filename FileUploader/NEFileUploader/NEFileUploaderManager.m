//
//  NEFileUploaderManager.m
//  FileUploader
//
//  Created by Chang Liu on 6/22/18.
//  Copyright © 2018 Next Entertainment. All rights reserved.
//

#import "NEFileUploaderManager.h"
#import <AFNetworking/AFNetworking.h>
NSString *const kDefaultCachePath = @"NEUploader/uploadFile";

@interface NEFileUploaderManager()


@end

@implementation NEFileUploaderManager

static NEFileUploaderManager *fileUploaderManager = nil;
+ (instancetype)shareManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        fileUploaderManager = [[self alloc] init];
    });
    return fileUploaderManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        // 获取本地没有完成的任务
        [self.missions addObjectsFromArray:[self fetchLocalMissions]];
    }
    return self;
}

+ (NSString *)fileCachePath {
    NSString *cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    if (fileUploaderManager.customCachePath.length) {
        NSString *fileCachePath = [NSString stringWithFormat:@"%@/%@",cachePath,fileUploaderManager.customCachePath];
        return fileCachePath;
    } else {
        NSString *fileCachePath = [NSString stringWithFormat:@"%@/%@",cachePath,kDefaultCachePath];
        return fileCachePath;
    }
}

+ (void)uploadWithMission:(NEFileUploadMission *)mission {
    NEFileUploaderManager *manager = [NEFileUploaderManager shareManager];
    [manager.missions addObject:mission];
    if (mission.filePath.length) {
        [manager sliceUploadWithMission:mission];
    }
}

#pragma mark -- public func
- (void)sliceUploadWithMission:(NEFileUploadMission *)mission {
    // 标记是不是最后一个切片，默认不是
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:mission.filePath];
    NSUInteger dataLength = handle.availableData.length;
    
    if (!handle) {
        // 如果资源不可用，停止
        return;
    }
    
    if (mission.uploadOffset.seek >= dataLength) {
        // 已经读到最后
        return;
    }
    
    // 从当前位置上传
    [handle seekToFileOffset:mission.uploadOffset.seek];
    NSData *sliceData = nil;
    if (dataLength <= mission.uploadOffset.seek + mission.uploadOffset.length) {
        // 当前是最后一个切片
        sliceData = [handle readDataToEndOfFile];
    } else {
        sliceData = [handle readDataOfLength:mission.uploadOffset.length];
    }
    
    if (sliceData.length) {
        NSURL *URL = [NSURL URLWithString:mission.toUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        __weak typeof(self) weakSelf = self;
        NSURLSessionUploadTask *uploadTask = [manager uploadTaskWithRequest:request
                                                                   fromData:sliceData
                                                                   progress:^(NSProgress * _Nonnull uploadProgress) {
                                                                       [mission setFileUploadStatus:NEFileUploadProcess];
                                                                   } completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                                                       if (error) {
                                                                           [mission setFileUploadStatus:NEFileUploadFaild];
                                                                           [weakSelf saveMission:mission];
                                                                       } else {
                                                                           // 每个切片上传完成都要更新一下本地状态，用于断点续传
                                                                           [mission setFileUploadStatus:NEFileUploadProcess];
                                                                           [mission setUploadOffset:NEUploadOffsetMake(mission.uploadOffset.seek + mission.uploadOffset.length, mission.uploadOffset.length)];
                                                                           [self saveMission:mission];
                                                                           [self sliceUploadWithMission:mission];
                                                                           if (mission.uploadProgress) {
                                                                               if (mission.uploadOffset.seek > dataLength) {
                                                                                   mission.uploadProgress(1, 1);
                                                                               } else {
                                                                                   mission.uploadProgress(1, (mission.uploadOffset.seek * 1.0 / dataLength));
                                                                               }
                                                                           }
                                                                       }
                                                                       [handle closeFile];
                                                                   }];
        [uploadTask resume];
    } else {
        [handle closeFile];
    }
}

- (void)deleteMission:(NEFileUploadMission *)mission {
    
}

#pragma mark -- getter
- (NSArray <NEFileUploadMission *>*)missions {
    if (!_missions) {
        _missions = [NSMutableArray new];
    }
    return _missions;
}


#pragma mark -- private
- (void)saveMission:(NEFileUploadMission *)mission {
    
}

- (NSArray <NEFileUploadMission *>*)fetchLocalMissions {
    return nil;
}

#pragma mark -- test
// 为了测试上传文件的完整和正确性，此方法是copy文件 
+ (void)testCopy:(NEFileUploadMission *)mission {
    NEFileUploaderManager *manager = [NEFileUploaderManager shareManager];
    [manager.missions addObject:mission];
    if (mission.filePath.length) {
        // 标记是不是最后一个切片，默认不是
        NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:mission.filePath];
        NSUInteger dataLength = handle.availableData.length;
        
        if (!handle) {
            // 如果资源不可用，停止
            return;
        }
        
        while (mission.uploadOffset.seek < dataLength) {
            [handle seekToFileOffset:mission.uploadOffset.seek];
            NSData *data = [handle readDataOfLength:mission.uploadOffset.length];
            [manager writeData:data ToFile:@"/Users/Alienchang/Downloads/DSC07495(test).ARW"];
            if ((dataLength - mission.uploadOffset.seek) < mission.uploadOffset.length) {
                [mission setUploadOffset:NEUploadOffsetMake(mission.uploadOffset.seek + (dataLength - mission.uploadOffset.seek), mission.uploadOffset.length)];
            } else {
                [mission setUploadOffset:NEUploadOffsetMake(mission.uploadOffset.seek + mission.uploadOffset.length, mission.uploadOffset.length)];
            }
            if (mission.uploadOffset.seek > dataLength) {
                mission.uploadProgress(1, 1);
            } else {
                mission.uploadProgress(1, (mission.uploadOffset.seek * 1.0 / dataLength));
            }
        }
    }
}

- (void)writeData:(NSData *)data
           ToFile:(NSString *)filePath {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filePath]) {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    NSFileHandle *writeHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
    [writeHandle seekToEndOfFile];
    [writeHandle writeData:data];
    [writeHandle closeFile];
}
@end
