//
//  NEFileUploadMission.m
//  FileUploader
//
//  Created by Chang Liu on 6/25/18.
//  Copyright © 2018 Next Entertainment. All rights reserved.
//

#import "NEFileUploadMission.h"

NEUploadOffset NEUploadOffsetMake(ByteOffset seek,ByteOffset length) {
    NEUploadOffset offset;
    offset.seek   = seek;
    offset.length = length;
    return offset;
}

@implementation NEFileUploadMission

+ (instancetype)missionWithTargetUrl:(NSString *)targetUrl
                            filePath:(NSString *)filePath
                      uploadProgress:(UploadProgressBlock)uploadProgress {
    NEFileUploadMission *fileUploadMission = [NEFileUploadMission new];
    [fileUploadMission setToUrl:targetUrl];
    [fileUploadMission setFilePath:filePath];
    [fileUploadMission setUploadProgress:uploadProgress];
    return fileUploadMission;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setMissionId:[[NSDate date] timeIntervalSince1970]];
    }
    return self;
}

- (void)setFileUploadStatus:(NEFileUploadStatus)fileUploadStatus {
    switch (fileUploadStatus) {
        case NEFileUploadProcess:
            {
                
            }
            break;
        case NEFileUploadPause:
        {
            
        }
            break;
        case NEFileUploadSuccess:
        {
            [self setProgress:1.0];
        }
            break;
        case NEFileUploadFaild:
        {
            
        }
            break;
        default:
            break;
    }
}

#pragma mark -- getter
- (NEUploadOffset)uploadOffset {
    if (!_uploadOffset.length) {
        _uploadOffset.seek = 0;
        _uploadOffset.length = 1024 * 1024;
    }
    return _uploadOffset;
}
@end
