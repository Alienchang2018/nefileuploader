//
//  NEFileUploadMission.h
//  FileUploader
//
//  Created by Chang Liu on 6/25/18.
//  Copyright © 2018 Next Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    NEFileUploadFaild,          // 上传失败
    NEFileUploadSuccess,        // 上传成功
    NEFileUploadPause,          // 暂停上传
    NEFileUploadProcess         // 正在上传
} NEFileUploadStatus;

typedef struct NEUploadOffset {
    ByteOffset seek, length;       // seek 起始点，length 上传的长度
} NEUploadOffset;


NEUploadOffset NEUploadOffsetMake(ByteOffset seek,ByteOffset length);

typedef void (^UploadProgressBlock)(float speed, float progress);

@interface NEFileUploadMission : NSObject

+ (instancetype)missionWithTargetUrl:(NSString *)targetUrl
                            filePath:(NSString *)filePath
                      uploadProgress:(UploadProgressBlock)uploadProgress;

// 任务id，默认当前时间戳
@property (nonatomic ,assign) NSUInteger missionId;
// 上传目标服务器
@property (nonatomic ,copy ,nonnull) NSString *toUrl;
// 上传偏移量，默认 {0 ,1M}
@property (nonatomic ,assign) NEUploadOffset uploadOffset;
// 当前已经上传的偏移量
@property (nonatomic ,assign) ByteOffset currentOffset;
// 上传文件的目录
@property (nonatomic ,copy ,nonnull)  NSString *filePath;
// 上传的文件名
@property (nonatomic ,copy)   NSString *fileName;
// 上传进程回调
@property (nonatomic ,copy)   UploadProgressBlock uploadProgress;
// 上传状态
@property (nonatomic ,assign) NEFileUploadStatus fileUploadStatus;
// 上传进度
@property (nonatomic ,assign) float progress;

@end
