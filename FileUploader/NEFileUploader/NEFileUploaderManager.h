//
//  NEFileUploaderManager.h
//  FileUploader
//
//  Created by Chang Liu on 6/22/18.
//  Copyright © 2018 Next Entertainment. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEFileUploadMission.h"

@interface NEFileUploaderManager : NSObject

// 当前任务数量
@property (nonatomic ,readonly) NSInteger currentMissionCount;
// 进行中的任务，初始化会获取本地没有完成的任务
@property (nonatomic ,strong) NSMutableArray *missions;
// 上传文件临时文件夹，在沙盒cache目录下拼接（MeMe\uploadFile）
@property (nonatomic ,copy)     NSString  *customCachePath;

// 上传文件临时文件夹，如果不设置customCachePath，返回默认
+ (NSString *)fileCachePath;
// 创建上传文件任务
+ (void)uploadWithMission:(NEFileUploadMission *)mission;
// 删除任务
- (void)deleteMission:(NEFileUploadMission *)mission;
// // 为了测试上传文件的完整和正确性，此方法是copy文件
+ (void)testCopy:(NEFileUploadMission *)mission;
@end
